{

	description = "Packages for making documents, with pandoc, texlive, and hopefully JATS.";

	inputs.nixpkgs.url = github:NixOS/nixpkgs/nixpkgs-unstable;

	outputs = { self, nixpkgs }: {

		packages.x86_64-linux =
		let
			pkgs = nixpkgs.legacyPackages.x86_64-linux;
		in {

			full-tex = pkgs.texlive.combine {inherit (pkgs.texlive) scheme-full; };

			con-tex = pkgs.texlive.combine {inherit (pkgs.texlive) scheme-context collection-metapost dejavu-otf; };

			fontz = pkgs.texlive.combine {inherit (pkgs.texlive) sourcecodepro sourcesanspro sourceserifpro plex-otf; };

			latex = pkgs.texlive.combine {inherit (pkgs.texlive) luatex scheme-small moderncv changepage;};

			pandoc-all = pkgs.buildEnv {
				name = "pandoc-all";
				paths = with pkgs; [
					(pkgs.texlive.combine {inherit (pkgs.texlive) scheme-small dejavu-otf fontspec luatex; })
					self.packages.x86_64-linux.fontz
					pandoc
					pandoc-imagine
					pandoc-plantuml-filter
					haskellPackages.pandoc-citeproc
				];
			};

			pandoc-context = pkgs.buildEnv {
				name = "pandoc-context";
				paths = with pkgs; [
					self.packages.x86_64-linux.con-tex
					pandoc
					pandoc-imagine
					pandoc-plantuml-filter
					haskellPackages.pandoc-citeproc
				];
			};

		};

	};

}
